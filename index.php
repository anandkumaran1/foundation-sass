<!DOCTYPE html> 
<html>
<head> 
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/foundation.css">
	<link rel="stylesheet" type="text/css" href="css/foundation-icons.css">
	<link rel="stylesheet" type="text/css" href="css/custom-styles.css">
	<title>Sass - foundation</title>
</head>
<body>

<div>
  new layout 
</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/foundation.min.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
  <script>
 
      $(document).foundation({
          accordion: { 
            callback : function (accordion) {
              // console.log(accordion);
            }
          }
      });
    </script>

<script src="//localhost:35729/livereload.js"></script> 

</body>
</html>
 